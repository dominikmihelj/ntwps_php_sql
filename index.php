<?php require 'dbConnection.php';
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>HARDVERAJ - Home</title>
    <meta name="keywords" content="CPU,PC,GPU,hardware,review,recenzije,news,novosti">
    <meta name="description" content="Novosti i recenzije PC hardware-a">
    <meta name="author" content="Dominik Mihelj">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="src/IP_generalLib.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>

<body>
    <header>
        <?php require 'navigation.php';?>
    </header>
    <main>
    <?php
if (isset($_GET['navigation'])) {
    $navigation = $_GET['navigation'];
    switch ($navigation) {
        case '2':
            include 'news.php';
            break;
        case '3':
            include 'contact.php';
            break;
        case '4':
            include 'aboutus.php';
            break;
        case '5':
            include 'gallery.php';
            break;
        case '6':
            include 'login.php';
            break;
        case '7':
            include 'registration.php';
            break;
        default:
            include 'home.php';
    }
}
?>
</main>
    <?php require 'footer.php';?>
</body>

</html>
